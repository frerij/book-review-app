from unicodedata import name
from django.urls import path

from reviews.views import list_reviews, create_review

urlpatterns = [
    path("", list_reviews, name="reviews_list"),
    path("new/", create_review, name="create_review"),
]